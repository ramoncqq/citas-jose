package com.ramon.peluqueriajose.security;


import com.ramon.peluqueriajose.model.Client;
import com.ramon.peluqueriajose.repository.ClientRepository;
import com.ramon.peluqueriajose.security.dto.JwtAuthenticationRefresh;
import com.ramon.peluqueriajose.security.dto.UserDTO;
import com.ramon.peluqueriajose.utils.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class CustomUserSecurityService implements AuthenticationProvider {

    private static Logger LOG = LoggerFactory.getLogger(CustomUserSecurityService.class);

    @Autowired
    private ClientRepository clientDAO;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    public Authentication authenticate(Authentication authentication) throws AuthenticationException, UsernameNotFoundException {
        Authentication auth = null;
        Client clientLogin = null;
        String email = authentication.getName();
        String password = authentication.getCredentials().toString();
        LOG.info("[Authenticate] -  email: {} password:{}", email, password);
        clientLogin = clientDAO.findByEmailAndPhone(email, password);
        if (clientLogin == null) {
            String errorMessage = String.format("usuario no encontrado %s", email);
            LOG.info("[Authenticate] usuario no encontrado {}", email);
            throw new UsernameNotFoundException(errorMessage);
        } else {
            if (!password.equals(clientLogin.getPhone())) throw new UsernameNotFoundException("Teléfono incorrecto");
            auth = new UsernamePasswordAuthenticationToken(clientLogin.getEmail(), password,
                    clientLogin.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList()));
            return auth;
        }
    }

    UserDTO loadUserByEmail(String email) throws UsernameNotFoundException {
        Client client = this.clientDAO.findByEmail(email);
        if (client == null) {
            throw new UsernameNotFoundException("Usuario no encontrado con el email : " + email);
        }
        return Mapper.convertClientToUserSecurity(client);
    }

    public JwtAuthenticationRefresh refreshToken(JwtAuthenticationRefresh refreshDTO) {
        JwtAuthenticationRefresh refreshTokenOUT = null;
        UserDTO userDTO = this.loadUserByEmail(jwtTokenProvider.getUserFromJWT(refreshDTO.getToken()));
        if (jwtTokenProvider.validateToken(refreshDTO.getToken()) && userDTO != null) {
            LOG.info("Se va a refrescar el token");
            SecurityContextHolder.clearContext();
            // Se vuelve a genear el authentication se mete en el contexto de spring security y se genera de nuevo el token
            Authentication authentication = new UsernamePasswordAuthenticationToken(userDTO.getUsername(), userDTO.getPassword());
            SecurityContextHolder.getContext().setAuthentication(authentication);
            refreshTokenOUT = JwtAuthenticationRefresh.builder().token(jwtTokenProvider.generateToken(authentication)).build();
        }
        return refreshTokenOUT;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}