package com.ramon.peluqueriajose.security.controller;

import com.ramon.peluqueriajose.security.CustomUserSecurityService;
import com.ramon.peluqueriajose.security.JwtTokenProvider;
import com.ramon.peluqueriajose.security.dto.JwtAuthenticationRefresh;
import com.ramon.peluqueriajose.security.dto.JwtAuthenticationRequest;
import com.ramon.peluqueriajose.security.dto.JwtAuthenticationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    private CustomUserSecurityService customUserSecurityService;


    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody JwtAuthenticationRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPhone()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return new ResponseEntity<>(new JwtAuthenticationResponse(jwt), HttpStatus.OK);
    }

    @PostMapping("/refresh")
    public ResponseEntity<?> refreshToken(@Valid @RequestBody JwtAuthenticationRefresh refreshRequest) {
        return new ResponseEntity<>(customUserSecurityService.refreshToken(refreshRequest), HttpStatus.OK);
    }
}
