package com.ramon.peluqueriajose.security.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

@Data
public class JwtAuthenticationRequest {

    @NotBlank
    private String email;

    @NotBlank
    private String phone;

}
