package com.ramon.peluqueriajose.security.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class JwtAuthenticationResponse implements Serializable {

    private final String token;

}
