package com.ramon.peluqueriajose.security.dto;

import lombok.Builder;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Data
@Builder
public class UserDTO implements UserDetails {

    private boolean isAccountNonExpired;

    private String username;

    private boolean isAccountNonLocked;

    private boolean isCredentialsNonExpired;

    private boolean isEnabled;

    private String password;

    private Collection<? extends GrantedAuthority> authorities;
}
