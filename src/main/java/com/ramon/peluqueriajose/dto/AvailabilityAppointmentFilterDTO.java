package com.ramon.peluqueriajose.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ramon.peluqueriajose.utils.LocalDateDeserializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AvailabilityAppointmentFilterDTO {

    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dateFrom;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dateTo;

    private Long hairdresser;

}
