package com.ramon.peluqueriajose.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ramon.peluqueriajose.dto.constants.StateEnum;
import com.ramon.peluqueriajose.utils.DateDeserializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AppointmentCreate {

    @JsonDeserialize(using = DateDeserializer.class)
    private Date date;

    @NotNull
    @NotEmpty
    private String email;

    @NotNull
    private Long hairdresser;

    @NotNull
    private Long service;

    private StateEnum state;
}
