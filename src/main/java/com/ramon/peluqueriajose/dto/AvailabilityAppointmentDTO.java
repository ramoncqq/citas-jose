package com.ramon.peluqueriajose.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AvailabilityAppointmentDTO {

    private String day;

    private Integer totalAppointments;

    private boolean full;

    private boolean holiday;

}
