package com.ramon.peluqueriajose.dto.error;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorInfo extends RuntimeException {

    private String message;

    private String description;

    private int statusCode;


}
