package com.ramon.peluqueriajose.dto.constants;

public class Constants {

    /**
     * NOTIFICATION PUSH
     **/
    public final static String ENDPOINT_NOTIFICATION_PUSH = "https://fcm.googleapis.com/fcm/send";
    public final static String NOTIFICATION_TITLE_APPOINTMENT_CREATE = "Solicitud de cita";
    public final static String NOTIFICATION_BODY_APPOINTMENT_CREATE = "%s ha pedido una cita para el %s";
    public final static String NOTIFICATION_TITLE_APPOINTMENT_CANCEL = "Cancelación de cita";
    public final static String NOTIFICATION_BODY_APPOINTMENT_CANCEL = "%s ha cancelado la cita el %s";
    public final static String NOTIFICATION_TITLE_APPOINTMENT_DATE = "Recordatorio de cita";
    public final static String NOTIFICATION_BODY_APPOINTMENT_DATE = "Le recordamos %s que tiene una cita mañana a las %s. Le recordamos que sino va acudir no olvide cancelar la cita mediante la aplicacion.";

    public final static String SEPARATOR_SCHEDULE = "-";
    public static final String FORMAT_DATE_APPOINTMENT = "yyyy-MM-dd";
    public static final String FORMAT_DATE_NOTIFICATION = "dd/MM/YYYY HH:mm";
    public static final String FORMAT_DATE_APPOINTMENT_WITH_HOUR_MINUTE = "yyyy-MM-dd HH:mm";
    public static final String FORMAT_HOUR_MINUTE = "HH:mm";

    /**
     * ERROS API
     **/
    public static final String ERROR_TITLE_CREATE_APPOINTMENT = "Crear cita";
    public static final String ERROR_DESCRIPTION_CREATE_APPOINTMENT_EXIST = "No se ha podido crear la cita ya que existe una a esa hora";
    public static final String ERROR_DESCRIPTION_CLIENT_NOT_FOUND_CREATE_APPOINTMENT = "No se ha podido encontrar al cliente";
    public static final String ERROR_DESCRIPTION_APPOINTMENT_ACTIVE_CREATE_APPOINTMENT = "No se ha podido crear la cita ya tienes una solicitada";
    public static final String ERROR_TITLE_CREATE_CLIENT = "Crear cliente";
    public static final String ERROR_DESCRIPTION_CREATE_CLIENT = "No se ha podido registrar los datos debido a que el teléfono o correo ya existe";
    public static final String ERROR_TITLE_CANCEL_APPOINTMENT = "Cancelar cita";
    public static final String ERROR_DESCRIPTION_CANCEL_APPOINTMENT = "Se ha producido un error a la hora de cancelar la cita";
    public static final String ERROR_TITLE_MODIFY_CLIENT = "Modificar cliente";
    public static final String ERROR_DESCRIPTION_MODIFY_CLIENT = "No se ha podido encontrar al cliente";
    public static final String ERROR_PHONE_DESCRIPTION_MODIFY_CLIENT = "El teléfono introducido ya existe en otro cliente.";
    public static final String ERROR_TITLE_HOLIDAY_CREATE = "Solicitar Vacaciones";
    public static final String ERROR_DESCRIPTION_HOLIDAY_CREATE = "No existe ningún peluquero";
    public static final String ERROR_TITLE_FINISH_APPOINTMENT = "Finalizar cita";
    public static final String ERROR_DESCRIPTION_FINISH_APPOINTMENT = "Se ha producido un error a la hora de finalizar la cita";
}
