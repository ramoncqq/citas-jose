package com.ramon.peluqueriajose.dto.constants;

public enum StateEnum {
    ACTIVA(0),
    CANCELADA(1),
    FINALIZADA(2);

    private Integer id;

    StateEnum(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public static StateEnum getById(Integer id) {
        StateEnum state = null;
        for (StateEnum e : values()) {
            if (e.id.equals(id)) state = e;
        }
        return state;
    }
}
