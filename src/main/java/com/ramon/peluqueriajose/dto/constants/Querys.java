package com.ramon.peluqueriajose.dto.constants;

public class Querys {

    public final static String ACTIVE_APPOINTMENT = "SELECT e from Appointment e where state.id=:stateID AND e.client.email=:email";
    public final static String APPOINTMENTS_BY_DAY_AND_STATE = "SELECT e from Appointment e where e.state.id=:stateID AND DATE(e.date)=DATE(:dateFrom)";
    public final static String APPOINTMENTS_EXISTS = "SELECT COUNT(a) from Appointment a where a.state.id IN :stateList AND a.date >= (:date) AND a.date <= (:date) AND a.hairdresser.id= :hairdresserId";
    public final static String CLIENT_EXITS = " SELECT COUNT(c) FROM Client c WHERE c.email=:email OR c.phone = :phone";
    public final static String PHONE_NUMBER_EXIST = " SELECT COUNT(c) FROM Client c WHERE c.email!=:email AND c.phone = :phone";
    public final static String HAIRDRESSER_APPOINTMENTS = "SELECT a from Appointment a where a.hairdresser.id= :hairdresserId AND DATE(a.date)=DATE(:dateFrom) ORDER BY a.state.id";
    public final static String HAIRDRESSER_APPOINTMENTS_BETWEEN_TWO_DATES = "SELECT a from Appointment a where a.hairdresser.id= :hairdresserId AND DATE(a.date)>=DATE(:dateFrom) AND  DATE(a.date)<=DATE(:dateTo)";
    public final static String CHANGE_STATE_APPOINTMENTS = "UPDATE Appointment a set a.state.id= :stateID where a.id IN :appointmentsIdsList";
}
