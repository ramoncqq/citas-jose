package com.ramon.peluqueriajose.dto.constants;

public enum RoleEnum {
    ADMIN(0),
    USER(1);

    private Integer id;

    RoleEnum(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }
}
