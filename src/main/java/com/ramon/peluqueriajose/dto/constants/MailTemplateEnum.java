package com.ramon.peluqueriajose.dto.constants;

public enum MailTemplateEnum {

    CANCEL("Cancelación de cita"),
    CREATE("Reserva de cita"),
    REMINDER("Recordatorio de cita"),
    CANCEL_MASSIVE("Cancelación de cita");

    private String title;

    MailTemplateEnum(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }
}
