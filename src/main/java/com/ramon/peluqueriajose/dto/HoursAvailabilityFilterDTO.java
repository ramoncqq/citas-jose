package com.ramon.peluqueriajose.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ramon.peluqueriajose.utils.LocalDateDeserializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HoursAvailabilityFilterDTO {

    @NotNull
    private Long hairdresserId;

    @NotNull
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dateIn;
}
