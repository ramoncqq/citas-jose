package com.ramon.peluqueriajose.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HousAvailabilityDTO {

    private List<String> morning;

    private List<String> afternoon;
}
