package com.ramon.peluqueriajose.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientFilter {

    private String email;

    private String name;

    private String surname;

}
