package com.ramon.peluqueriajose.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientDetail {

    private String email;

    private String name;

    private String surname;

    private String phone;

    private boolean active;

    private Long hairdresserId;

    private String fcmToken;

    private Boolean notification;

}
