package com.ramon.peluqueriajose.dto;

import com.ramon.peluqueriajose.dto.constants.Constants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HairdresserHolidayDTO {

    @DateTimeFormat(pattern = Constants.FORMAT_DATE_APPOINTMENT)
    @NotNull
    private Date startDate;

    @NotNull
    private Long hairdresserId;
}
