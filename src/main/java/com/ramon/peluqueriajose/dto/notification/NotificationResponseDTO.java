package com.ramon.peluqueriajose.dto.notification;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class NotificationResponseDTO {

    @JsonProperty("multicast_id")
    private Long multicastId;

    private Integer success;

    private Integer failure;

    @JsonProperty("canonical_ids")
    private Integer canonicalIds;

    private List<NotificationResultsDTO> results;

}
