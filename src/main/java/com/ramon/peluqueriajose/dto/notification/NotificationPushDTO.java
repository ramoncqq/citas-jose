package com.ramon.peluqueriajose.dto.notification;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NotificationPushDTO {

    private String to;

    private NotificationDTO notification;

    @Builder.Default
    private String priority = "high";

}
