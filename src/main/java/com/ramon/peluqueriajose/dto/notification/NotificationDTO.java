package com.ramon.peluqueriajose.dto.notification;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NotificationDTO {

    private String title;

    private String body;

    @Builder.Default
    private String sound = "default";

}
