package com.ramon.peluqueriajose.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ramon.peluqueriajose.dto.constants.StateEnum;
import com.ramon.peluqueriajose.utils.DateDeserializer;
import com.ramon.peluqueriajose.utils.DateSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AppointmentDTO {

    private Long id;

    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    private Date date;

    private String hairdresserName;

    private String serviceName;

    private StateEnum state;

    private String name;

    private String surname;

    private String email;

}
