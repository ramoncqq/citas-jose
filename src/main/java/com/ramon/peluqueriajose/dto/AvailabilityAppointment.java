package com.ramon.peluqueriajose.dto;

public interface AvailabilityAppointment {

    String getDay();

    Integer getTotalAppointments();

}
