package com.ramon.peluqueriajose.service;

import com.ramon.peluqueriajose.config.AppConfig;
import com.ramon.peluqueriajose.dto.AvailabilityAppointmentDTO;
import com.ramon.peluqueriajose.dto.AvailabilityAppointmentFilterDTO;
import com.ramon.peluqueriajose.dto.HoursAvailabilityFilterDTO;
import com.ramon.peluqueriajose.dto.HousAvailabilityDTO;
import com.ramon.peluqueriajose.dto.constants.Constants;
import com.ramon.peluqueriajose.dto.constants.StateEnum;
import com.ramon.peluqueriajose.dto.error.ErrorInfo;
import com.ramon.peluqueriajose.model.Appointment;
import com.ramon.peluqueriajose.model.Holiday;
import com.ramon.peluqueriajose.model.Schedule;
import com.ramon.peluqueriajose.repository.AppointmentRepository;
import com.ramon.peluqueriajose.utils.UtilDates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AvailabilityService {

    private Logger LOGGER = LoggerFactory.getLogger(AvailabilityService.class);

    private static final int MINUTES_PER_HOUR = 60;

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private HairdresserService hairdresserService;

    @Autowired
    private AppointmentRepository appointmentDAO;

    /**
     * Method that find availability appointments of the month
     *
     * @param filterDTO
     * @return
     */
    public List<AvailabilityAppointmentDTO> getAvailabilityAppointmentsBetweenTwoDates(AvailabilityAppointmentFilterDTO filterDTO) {
        List<AvailabilityAppointmentDTO> avilabilityDtosList = new ArrayList<>();
        LOGGER.info("Se va a consultar la disponibilidad de citas {}", filterDTO);

        // Se obtienen las vacaciones del peluquero
        Set<Holiday> holidayList = hairdresserService.getHolidays(filterDTO.getHairdresser());

        // Se obtienen las citas entre un rango de fechas
        List<Appointment> appointmentList =
                appointmentDAO.getAppointmentsBetweenTwoDates(filterDTO.getHairdresser(),
                        UtilDates.convertLocalDateToTimeStampSQL(filterDTO.getDateFrom()),
                        UtilDates.convertLocalDateToTimeStampSQL(filterDTO.getDateTo()));

        // Mientras que la fecha del inicio sea menor que la fecha de fin
        while (filterDTO.getDateFrom().isBefore(filterDTO.getDateTo().plusDays(1))) {

            // Se calcula el numero de citas para ese dia
            final Double numbersAppointmentPerDay = scheduleService.calculateWorkingHours(filterDTO.getDateFrom().getDayOfWeek().getValue())
                    * (MINUTES_PER_HOUR / appConfig.getHourAppointment());

            // Se filtran las citas de ese dia
            List<Appointment> appointmentsCurrentList =
                    appointmentList.stream().filter(appointment -> UtilDates.convertDateToLocalDate(appointment.getDate()).isEqual(filterDTO.getDateFrom()))
                            .collect(Collectors.toList());

            // Se comprueba si ese día el peluquero está de vacaciones.
            boolean isHoliday = false;
            for (Holiday holiday : holidayList) {
                if (!isHoliday) {
                    Date currentDay = UtilDates.convertLocalDateToDate(filterDTO.getDateFrom());
                    isHoliday = UtilDates.checkBetween(currentDay, holiday.getStartDate(), holiday.getFinishDate());
                }
            }


            Integer totalAppointments = this.calculateNumberAppointmentsAvailability(appointmentsCurrentList, filterDTO.getDateFrom(), numbersAppointmentPerDay);

            avilabilityDtosList.add(
                    AvailabilityAppointmentDTO.builder()
                            .day(filterDTO.getDateFrom().toString())
                            .full(totalAppointments >= numbersAppointmentPerDay)
                            .totalAppointments(totalAppointments)
                            .holiday(isHoliday)
                            .build());

            // Se incrementa en uno la fecha de inicio
            filterDTO.setDateFrom(filterDTO.getDateFrom().plusDays(1));
        }

        return avilabilityDtosList;
    }

    /**
     * Method that returns availability hours from a date
     *
     * @param filter
     * @return appointments available
     * @throws ErrorInfo
     */
    public HousAvailabilityDTO getAvailableHoursFromDate(HoursAvailabilityFilterDTO filter) throws ErrorInfo {
        List<String> morning = new ArrayList<>();
        List<String> afternoon = new ArrayList<>();

        // Se obtiene el horario en función del dia
        Schedule scheduleDTO = scheduleService.getSchedule(filter.getDateIn().getDayOfWeek().getValue());
        Timestamp dateDB = Timestamp.valueOf(filter.getDateIn().atStartOfDay());

        List<Appointment> appointmentList = appointmentDAO.getAppointmentsByHairdresserIdAndDate(filter.getHairdresserId(), dateDB);
        LOGGER.info("[getHoursAvailability] {} CITAS OBTENIDAS {}", appointmentList.size(), dateDB);

        // Si hay horario en la mañana
        if (!StringUtils.isEmpty(scheduleDTO.getMorning())) {
            morning.addAll(this.getAvailableHours(appointmentList, scheduleDTO.getMorning(), filter.getDateIn()));
        }

        // Si hay horario en la tarde
        if (!StringUtils.isEmpty(scheduleDTO.getAfternoon())) {
            afternoon.addAll(this.getAvailableHours(appointmentList, scheduleDTO.getAfternoon(), filter.getDateIn()));
        }

        return HousAvailabilityDTO.builder().morning(morning).afternoon(afternoon).build();
    }

    /**
     * Method that returns available hours
     *
     * @param appointmentList
     * @param hourSchedule
     * @return
     */
    private List<String> getAvailableHours(List<Appointment> appointmentList, String hourSchedule, LocalDate dateSelected) {
        List<String> appointmentsAvailable = new ArrayList<>();

        // Se obtiene la hora de Entrada / salida
        String[] hours = hourSchedule.split(Constants.SEPARATOR_SCHEDULE);
        LocalTime hourBegin = UtilDates.convertStringToLocalTime(hours[0]);
        LocalTime hourEnd = UtilDates.convertStringToLocalTime(hours[1]);
        boolean today = dateSelected.equals(LocalDate.now());

        // Hasta que la fecha no sea la de cierre
        while (hourBegin.isBefore(hourEnd)) {
            // Se comprueba si exista una cita en esa hora
            boolean existAppointmentInCurrentHour = false;
            boolean appointmentCanceled = false;
            for (Appointment appointment : appointmentList) {
                if (!existAppointmentInCurrentHour) {
                    existAppointmentInCurrentHour = UtilDates.checkLocalTime(appointment.getDate(), hourBegin);
                    // Se comprueba que exista una hora en esa cita y que si es cancelada y posterior a la fecha actual
                    if (existAppointmentInCurrentHour && appointment.getState().getId().equals(StateEnum.CANCELADA.getId()) && appointment.getDate().after(new Date())) {
                        LOGGER.info("Es la cita cancelada de las {}", hourBegin);
                        appointmentCanceled = true;
                    }
                }
            }
            // Sino hay ninguna cita a esa hora o es una cita cancelada
            if ((!existAppointmentInCurrentHour || appointmentCanceled)) {
                // Si el dia seleccionado no es hoy se añade y si es hoy se comprueba la hora
                if (!today) {
                    appointmentsAvailable.add(hourBegin.toString());
                } else if (hourBegin.isAfter(LocalTime.now())) {
                    appointmentsAvailable.add(hourBegin.toString());
                }
            }

            hourBegin = hourBegin.plusMinutes(appConfig.getHourAppointment());
        }
        LOGGER.info("[getAvailableAppointments] HORAS DISPONIBLES {}", appointmentsAvailable);

        return appointmentsAvailable;
    }

    /**
     * Method that calculate numbers appointments current (canceled, not exist)
     *
     * @param appointmentsCurrentList
     * @param date
     * @return
     */
    private Integer calculateNumberAppointmentsAvailability(List<Appointment> appointmentsCurrentList, LocalDate date, Double numbersAppointmentPerDay) {
        Integer totalAppointments = 0;
        Schedule scheduleDTO = scheduleService.getSchedule(date.getDayOfWeek().getValue());

        if (scheduleDTO != null) {
            totalAppointments = appointmentsCurrentList.size();
            // Se comprueba si hay citas canceladas y en caso de que haya si existe otra a esa hora se resta al total de citas.
            totalAppointments -= this.calculateAppointmentsCanceled(appointmentsCurrentList);

            // En caso de que sea hoy se tienen en cuenta las citas que no hayan existido
            if (date.equals(LocalDate.now())) {

                // Se calculan la cantidad de citas perdidas por la mañana
                if (totalAppointments <= numbersAppointmentPerDay && scheduleDTO.getMorning() != null) {
                    totalAppointments += calculateAppointmentsNotExist(appointmentsCurrentList, scheduleDTO.getMorning());
                }

                // Se calculan la cantidad de citas perdidas por la tarde
                if (totalAppointments <= numbersAppointmentPerDay && scheduleDTO.getAfternoon() != null) {
                    totalAppointments += calculateAppointmentsNotExist(appointmentsCurrentList, scheduleDTO.getAfternoon());
                }
            }
        } else {
            // Si es domingo se pone el máximo
            totalAppointments = numbersAppointmentPerDay.intValue();
        }

        return totalAppointments;
    }

    /**
     * Method that calculate current appointments
     *
     * @param appointmentsCurrentList
     * @returns totalAppointments
     */
    private Integer calculateAppointmentsNotExist(List<Appointment> appointmentsCurrentList, String hourSchedule) {

        // Se obtiene la hora de Entrada / salida
        String[] hours = hourSchedule.split(Constants.SEPARATOR_SCHEDULE);
        LocalTime hourBegin = UtilDates.convertStringToLocalTime(hours[0]);
        LocalTime hourEnd = UtilDates.convertStringToLocalTime(hours[1]);

        Integer totalAppointmentsNotExist = 0;

        while (hourBegin.isBefore(hourEnd)) {
            final LocalTime hourBeginFinal = hourBegin;
            Long exist = appointmentsCurrentList.stream().filter(appointmentFilter -> UtilDates.checkLocalTime(appointmentFilter.getDate(), hourBeginFinal)).count();
            // En caso de que no exista una cita a esa hora y además sea superior a la fecha actual se suma uno
            if (exist <= 0 && hourBegin.isBefore(LocalTime.now())) {
                totalAppointmentsNotExist += 1;
                LOGGER.info("Cita No existente a las hora {}", hourBegin);
            }
            hourBegin = hourBegin.plusMinutes(appConfig.getHourAppointment());
        }

        return totalAppointmentsNotExist;
    }

    /**
     * Method that calculate appointmentsCanceled.
     *
     * @param appointmentsCurrentList
     * @return
     */
    private Integer calculateAppointmentsCanceled(List<Appointment> appointmentsCurrentList) {
        Integer totalAppointmentsCanceled = 0;

        for (Appointment appointment : appointmentsCurrentList) {
            if (appointment.getState().getId().equals(StateEnum.CANCELADA.getId())) {
                // Se comprueba si la hora de la cita cancelada hay otra
                Long existOtherAppointment = appointmentsCurrentList.stream().filter(appointmentFilter ->
                        appointmentFilter.getDate().equals(appointment.getDate()) && !appointmentFilter.getState().getId().equals(StateEnum.CANCELADA.getId())).count();
                if (existOtherAppointment > 0) {
                    LOGGER.info("Existe otra cita a la hora de la cita cancelada {}", appointment.getDate());
                    totalAppointmentsCanceled += 1;
                }
            }
        }

        return totalAppointmentsCanceled;
    }

}