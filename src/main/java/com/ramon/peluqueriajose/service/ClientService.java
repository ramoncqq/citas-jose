package com.ramon.peluqueriajose.service;

import com.ramon.peluqueriajose.dto.AppointmentDTO;
import com.ramon.peluqueriajose.dto.ClientDetail;
import com.ramon.peluqueriajose.dto.ClientFilter;
import com.ramon.peluqueriajose.dto.constants.Constants;
import com.ramon.peluqueriajose.dto.error.ErrorInfo;
import com.ramon.peluqueriajose.dto.constants.RoleEnum;
import com.ramon.peluqueriajose.dto.constants.StateEnum;
import com.ramon.peluqueriajose.model.Appointment;
import com.ramon.peluqueriajose.model.Client;
import com.ramon.peluqueriajose.model.Hairdresser;
import com.ramon.peluqueriajose.model.Role;
import com.ramon.peluqueriajose.model.State;
import com.ramon.peluqueriajose.repository.ClientRepository;
import com.ramon.peluqueriajose.utils.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientService {

    private Logger LOGGER = LoggerFactory.getLogger(ClientService.class);

    @Autowired
    private ClientRepository clientDAO;

    /**
     * Method that return a client by email
     *
     * @param email
     * @return
     */
    public Client getClientByEmail(String email) {
        LOGGER.info("[getClientByEmail] se va a obtener el cliente {}", email);
        return clientDAO.findByEmail(email);
    }

    /**
     * Method that find a client
     *
     * @param email
     * @return
     */
    public ClientDetail getClientDetailByEmail(String email) {
        Client client = clientDAO.findByEmail(email);
        ClientDetail clientDetail = new ClientDetail();
        if (client != null) {
            clientDetail = Mapper.convertClientDBToClientDetail(client);
        }
        LOGGER.info("[getClientByEmail] Cliente obtenido {}", client);
        return clientDetail;
    }

    /**
     * Method that create a client
     *
     * @param clientDetail
     * @return
     * @throws ErrorInfo
     */
    @Transactional
    public ClientDetail createClient(ClientDetail clientDetail) throws ErrorInfo {
        Client clientDB = new Client();
        if (clientDAO.findByEmailOrPhone(clientDetail.getEmail(), clientDetail.getPhone()) > 0) {
            LOGGER.error("[createClient] el cliente {} con teléfono {} ya existe", clientDetail.getEmail(), clientDetail.getPhone());
            throw new ErrorInfo(Constants.ERROR_TITLE_CREATE_CLIENT, Constants.ERROR_DESCRIPTION_CREATE_CLIENT,
                    HttpStatus.INTERNAL_SERVER_ERROR.value());
        } else {
            clientDB = Mapper.convertClientDetailToClient(clientDetail);
            // Se setean los campos por defecto (Activo, Rol Usuario y el peluquero)
            Role role = Role.builder().id(RoleEnum.USER.getId()).build();
            clientDB.setIsHairdresser(Boolean.FALSE);
            clientDB.setActive(Boolean.TRUE);
            clientDB.setRoles(new HashSet<>(Arrays.asList(role)));
            clientDB.setHairdresser(Hairdresser.builder().id(0L).build());
            clientDB = clientDAO.save(clientDB);
            LOGGER.info("[createClient] se ha creado el cliente {}", clientDB);
        }

        return Mapper.convertClientDBToClientDetail(clientDB);
    }

    /**
     * Method that modify a client
     *
     * @param clientDetail
     * @return
     * @throws ErrorInfo
     */
    @Transactional
    public ClientDetail modifyClient(ClientDetail clientDetail) throws ErrorInfo {
        Client clientDB = getClientByEmail(clientDetail.getEmail());
        if (clientDB == null) {
            LOGGER.error("[modifyClient] no se ha podido encontrar al cliente {}", clientDetail.getEmail());
            throw new ErrorInfo(Constants.ERROR_TITLE_MODIFY_CLIENT, Constants.ERROR_DESCRIPTION_MODIFY_CLIENT,
                    HttpStatus.NOT_FOUND.value());
        } else {
            if (clientDAO.existsPhoneInOtherClient(clientDetail.getEmail(), clientDetail.getPhone()) > 0) {
                LOGGER.error("[modifyClient] no se ha podido encontrar al cliente {}", clientDetail.getEmail());
                throw new ErrorInfo(Constants.ERROR_TITLE_MODIFY_CLIENT, Constants.ERROR_PHONE_DESCRIPTION_MODIFY_CLIENT,
                        HttpStatus.NOT_FOUND.value());
            } else {
                Mapper.modifyClientDB(clientDetail, clientDB);
                // En caso de que sea un peluquero se modifica el token, el nombre, notificaciones y el correo
                if (clientDB.getIsHairdresser()) {
                    clientDB.getHairdresser().setFcmToken(clientDetail.getFcmToken());
                    clientDB.getHairdresser().setName(clientDetail.getName());
                    clientDB.getHairdresser().setNotification(clientDetail.getNotification());
                    clientDB.getHairdresser().setEmail(clientDetail.getEmail());
                }
                clientDB = clientDAO.save(clientDB);
                LOGGER.info("[modifyClient] se ha modficado el cliente {}", clientDB);
            }
        }

        return Mapper.convertClientDBToClientDetail(clientDB);
    }

    /**
     * Method that find a client by phone
     *
     * @param phone
     * @return
     */
    public ClientFilter findByPhone(String phone) {
        Client client = clientDAO.findByPhone(phone);
        ClientFilter result = new ClientFilter();

        if (client != null) {
            result = ClientFilter.builder().name(client.getName()).surname(client.getSurname()).
                    email(client.getEmail()).build();
        } else {
            LOGGER.error("[findByPhone] no se ha encontrado al cliente con telefono {}", phone);
            throw new ErrorInfo("OBTENER CLIENTE", "No se ha podido encontrar al cliente", HttpStatus.NOT_FOUND.value());
        }

        return result;
    }

    /**
     * Method that lock user and finish appointment active.
     *
     * @param email
     */
    @Transactional
    public ClientDetail lockUser(String email) {
        Client client = this.getClientByEmail(email);
        if (client != null) {
            // Obtengo la cita activa para cambiarla a canceladas.
            client.getAppointments().stream()
                    .filter(appointment -> appointment.getState().getId().equals(StateEnum.ACTIVA.getId()))
                    .forEach(appointment -> appointment.setState(State.builder().id(StateEnum.CANCELADA.getId()).build()));
            client.setActive(Boolean.FALSE);
            LOGGER.info("[lockUser] Se va a dar de baja el usuario {} y la cita que tenga activa", email);
            client = this.clientDAO.save(client);
        } else {
            LOGGER.error("[lockUser] no se ha encontrado al cliente {}", email);
            throw new ErrorInfo("Bloquear cliente", "No se ha podido encontrar al cliente", HttpStatus.NOT_FOUND.value());
        }
        return Mapper.convertClientDBToClientDetail(client);
    }

    /**
     * Method that lock user and finish appointment active.
     *
     * @param email
     */
    @Transactional
    public ClientDetail unlockUser(String email) {
        Client client = this.getClientByEmail(email);
        if (client != null) {
            // Obtengo la cita activa para cambiarla a canceladas.
            client.setActive(Boolean.TRUE);
            LOGGER.info("[unlockUser] Se va a activar la cuenta del usuario {}", email);
            client = this.clientDAO.save(client);
        } else {
            LOGGER.error("[unlockUser] no se ha encontrado al cliente {}", email);
            throw new ErrorInfo("Desbloquear cliente", "No se ha podido encontrar al cliente", HttpStatus.NOT_FOUND.value());
        }
        return Mapper.convertClientDBToClientDetail(client);
    }

    /**
     * Method that obtains user appointments
     *
     * @param email
     * @return
     */
    public List<AppointmentDTO> getAppointmentsByEmail(String email) {
        Client client = this.getClientByEmail(email);
        List<AppointmentDTO> appointmentDTOS = new ArrayList<>();
        if (client != null) {
            LOGGER.info("Se han obtenido {} citas para el usuario {}", client.getAppointments().size(), email);
            List<Appointment> appointmentList = client.getAppointments().stream()
                    .sorted(Comparator.comparing(Appointment::getDate).reversed())
                    .collect(Collectors.toList());

            appointmentList.forEach(appointment ->
                    appointmentDTOS.add(Mapper.convertAppointDBToAppointmentDTO(appointment)));
        }
        return appointmentDTOS;
    }

    /**
     * Method that obtains appointments user from phone
     *
     * @param phone
     * @return
     */
    public List<AppointmentDTO> getAppointmentsByPhone(String phone) {
        List<AppointmentDTO> appointmentDTOS = new ArrayList<>();
        Client client = this.clientDAO.findByPhone(phone);
        if (client != null) {
            LOGGER.info("Se han obtenido {} citas para el usuario con telefono {}", client.getAppointments().size(), phone);
            List<Appointment> appointmentList = client.getAppointments().stream()
                    .sorted(Comparator.comparing(Appointment::getDate).reversed())
                    .collect(Collectors.toList());

            appointmentList.forEach(appointment -> appointmentDTOS.add(Mapper.convertAppointDBToAppointmentDTO(appointment)));
        }
        return appointmentDTOS;
    }

}
