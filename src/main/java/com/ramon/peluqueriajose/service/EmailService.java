package com.ramon.peluqueriajose.service;

import com.ramon.peluqueriajose.config.AppConfig;
import com.ramon.peluqueriajose.dto.MailDTO;
import com.ramon.peluqueriajose.dto.constants.Constants;
import com.ramon.peluqueriajose.dto.constants.MailTemplateEnum;
import com.ramon.peluqueriajose.model.Appointment;
import com.ramon.peluqueriajose.utils.UtilDates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;

@Service
public class EmailService {

    private static Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private SpringTemplateEngine templateEngine;

    /**
     * Method that managment type of email
     * @param mailTemplateEnum
     * @param appointment
     * @throws MessagingException
     */
    public void handleEmail(MailTemplateEnum mailTemplateEnum, Appointment appointment) throws MessagingException {
        MailDTO mailDTO = new MailDTO();
        String fullName = appointment.getClient().getName() + " " + appointment.getClient().getSurname();

        // Se setea el asunto y el title
        mailDTO.setSubject(mailTemplateEnum.getTitle());
        mailDTO.getVariables().put("title", mailTemplateEnum.getTitle());

        switch (mailTemplateEnum) {
            case CANCEL:
                mailDTO.getVariables().put("body", String.format(Constants.NOTIFICATION_BODY_APPOINTMENT_CANCEL, fullName,
                        UtilDates.convertDateToStringNotification(appointment.getDate())));
                mailDTO.setTo(appointment.getHairdresser().getEmail());
                mailDTO.getVariables().put("title", mailTemplateEnum.getTitle());
                break;
            case REMINDER:
                mailDTO.getVariables().put("body", String.format(Constants.NOTIFICATION_BODY_APPOINTMENT_DATE, fullName,
                        UtilDates.convertDateToStringNotification(appointment.getDate())));
                mailDTO.setTo(appointment.getClient().getEmail());
                mailDTO.getVariables().put("title", mailTemplateEnum.getTitle());
                break;
            case CREATE:
                mailDTO.getVariables().put("body", String.format(Constants.NOTIFICATION_BODY_APPOINTMENT_CREATE, fullName,
                        UtilDates.convertDateToStringNotification(appointment.getDate())));
                mailDTO.setTo(appointment.getHairdresser().getEmail());
                mailDTO.getVariables().put("title", mailTemplateEnum.getTitle());
                break;
            case CANCEL_MASSIVE:
                String body = String.format("%s ha cancelado la cita del %s debido a %s", appointment.getHairdresser().getName(),
                        UtilDates.convertDateToStringNotification(appointment.getDate()), appointment.getReason());
                mailDTO.getVariables().put("body", body);
                mailDTO.setTo(appointment.getClient().getEmail());
                mailDTO.getVariables().put("title", mailTemplateEnum.getTitle());
                break;
        }

        this.sendEmail(mailDTO);
    }

    /**
     * Method that send email
     *
     * @param mailDTO
     * @throws MessagingException
     */
    private void sendEmail(MailDTO mailDTO) throws MessagingException {
        LOGGER.info("Se va e enviar el correo a {} con asunto {}", mailDTO.getTo(), mailDTO.getSubject());
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());

        // Se añaden las variables
        Context context = new Context();
        context.setVariables(mailDTO.getVariables());
        context.setVariable("host", appConfig.getHost());

        String htmlParse = templateEngine.process("email-template", context);

        helper.setTo(mailDTO.getTo());
        helper.setSubject(mailDTO.getSubject());
        helper.setText(htmlParse, true);
        emailSender.send(message);
    }


}
