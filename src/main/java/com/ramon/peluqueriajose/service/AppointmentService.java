package com.ramon.peluqueriajose.service;

import com.ramon.peluqueriajose.dto.AppointmentCancelMassiveDTO;
import com.ramon.peluqueriajose.dto.AppointmentCreate;
import com.ramon.peluqueriajose.dto.AppointmentDTO;
import com.ramon.peluqueriajose.dto.AppointmentHairdresserFilterDTO;
import com.ramon.peluqueriajose.dto.constants.MailTemplateEnum;
import com.ramon.peluqueriajose.model.Client;
import com.ramon.peluqueriajose.model.Hairdresser;
import com.ramon.peluqueriajose.model.State;
import com.ramon.peluqueriajose.dto.constants.Constants;
import com.ramon.peluqueriajose.model.Appointment;
import com.ramon.peluqueriajose.dto.error.ErrorInfo;
import com.ramon.peluqueriajose.repository.AppointmentRepository;

import com.ramon.peluqueriajose.dto.constants.StateEnum;
import com.ramon.peluqueriajose.utils.Mapper;
import com.ramon.peluqueriajose.utils.UtilDates;
import org.hibernate.StatelessSessionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AppointmentService {

    private Logger LOGGER = LoggerFactory.getLogger(AppointmentService.class);

    @Autowired
    private AppointmentRepository appointmentDAO;

    @Autowired
    private ClientService clientService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private HairdresserService hairdresserService;


    /**
     * Method that create an Appoinment.
     *
     * @param appointmentCreate
     * @return
     * @throws ErrorInfo
     */
    public AppointmentCreate createAppointment(AppointmentCreate appointmentCreate, Boolean isAdmin) throws ErrorInfo {
        Appointment appointment = null;
        List<Integer> stateList = Arrays.asList(StateEnum.ACTIVA.getId(), StateEnum.FINALIZADA.getId());
        Timestamp dateSQL = UtilDates.convertDateToTimeStampSQL(appointmentCreate.getDate());

        // Se comprueba que no haya una cita a esa hora.
        if (appointmentDAO.checkExistAppointment(stateList, dateSQL, appointmentCreate.getHairdresser()) > 0) {
            LOGGER.error("[createAppointment] Ya existe una cita a esa hora {}", appointmentCreate.getDate());
            throw new ErrorInfo(Constants.ERROR_TITLE_CREATE_APPOINTMENT,
                    Constants.ERROR_DESCRIPTION_CREATE_APPOINTMENT_EXIST, HttpStatus.NOT_FOUND.value());
        }

        // Se comprueba que exista el usuario
        Client client = clientService.getClientByEmail(appointmentCreate.getEmail());
        if (client == null) {
            LOGGER.error("[createAppointment] El usuario {} no se encuentra", appointmentCreate.getEmail());
            throw new ErrorInfo(Constants.ERROR_TITLE_CREATE_APPOINTMENT,
                    Constants.ERROR_DESCRIPTION_CLIENT_NOT_FOUND_CREATE_APPOINTMENT, HttpStatus.NOT_FOUND.value());
        }

        // Se comprueba que no haya citas activas para el usuario en caso de que no sea un administrador
        if (!isAdmin) {
            Appointment appointmentActive = appointmentDAO.getActiveAppointmentUser(StateEnum.ACTIVA.getId(), client.getEmail());
            if (appointmentActive != null) {
                LOGGER.error("[createAppointment] El usuario {} ya tiene una cita activa", appointmentActive.getClient().getEmail());
                throw new ErrorInfo(Constants.ERROR_TITLE_CREATE_APPOINTMENT,
                        Constants.ERROR_DESCRIPTION_APPOINTMENT_ACTIVE_CREATE_APPOINTMENT, HttpStatus.INTERNAL_SERVER_ERROR.value());
            }
        }

        appointment = Appointment.builder().state(State.builder().id(StateEnum.ACTIVA.getId()).build())
                .client(client).date(appointmentCreate.getDate())
                .service(com.ramon.peluqueriajose.model.Service.builder().id(appointmentCreate.getService()).build())
                .hairdresser(Hairdresser.builder().id(appointmentCreate.getHairdresser()).build())
                .build();
        appointmentCreate.setState(StateEnum.ACTIVA);
        appointment = appointmentDAO.save(appointment);
        LOGGER.info("[createAppointment] Se ha creado la cita {} ", appointment);

        // Se envia la notificacion PUSH y el envío de correo
        Hairdresser hairdresser = this.hairdresserService.getHairdresserById(appointmentCreate.getHairdresser());
        appointment.getHairdresser().setEmail(hairdresser.getEmail());
        final Appointment appointmentFinal = appointment;
        new Thread(() -> {
            try {
                notificationService.sendNotificationToAdmin(Constants.NOTIFICATION_TITLE_APPOINTMENT_CREATE,
                        Constants.NOTIFICATION_BODY_APPOINTMENT_CREATE, appointmentFinal);
                emailService.handleEmail(MailTemplateEnum.CREATE, appointmentFinal);
            } catch (MessagingException e) {
                LOGGER.error("[createAppointment] Se ha producido un error al enviar el email:{} ", e);
            }
        }).start();

        return appointmentCreate;
    }

    /**
     * Method that finish an appointment.
     *
     * @param idAppointment
     * @return
     * @throws ErrorInfo
     */
    public AppointmentDTO finishAppointment(Long idAppointment) throws ErrorInfo {
        Appointment appointmentActive = appointmentDAO.findOne(idAppointment);
        if (appointmentActive != null) {
            appointmentActive.setState(State.builder().id(StateEnum.FINALIZADA.getId()).build());
            appointmentActive = appointmentDAO.save(appointmentActive);
            LOGGER.info("[finishAppointment] Se ha finalizado la cita del usuario {}", appointmentActive.getClient().getEmail());
        } else {
            LOGGER.error("[finishAppointment] No existe ninguna cita con el id {}", idAppointment);
            throw new ErrorInfo(Constants.ERROR_TITLE_FINISH_APPOINTMENT, Constants.ERROR_DESCRIPTION_FINISH_APPOINTMENT, HttpStatus.NOT_FOUND.value());
        }
        return Mapper.convertAppointDBToAppointmentDTO(appointmentActive);
    }

    /**
     * Method that cancel an appointment.
     *
     * @param idAppointment
     * @return
     * @throws ErrorInfo
     */
    public AppointmentDTO cancelAppointment(Long idAppointment) throws ErrorInfo {
        // Se comprueba que exista la cita activa
        Appointment appointmentActive = appointmentDAO.findOne(idAppointment);
        if (appointmentActive != null) {
            appointmentActive.setState(State.builder().id(StateEnum.CANCELADA.getId()).build());
            appointmentActive = appointmentDAO.save(appointmentActive);
            LOGGER.info("[cancelAppointment] El usuario {} ha cancelado la cita", appointmentActive.getClient().getEmail());

            // Se envia la notificacion PUSH y el envío de correo
            final Appointment appointmentFinal = appointmentActive;
            new Thread(() -> {
                try {
                    notificationService.sendNotificationToAdmin(Constants.NOTIFICATION_TITLE_APPOINTMENT_CANCEL, Constants.NOTIFICATION_BODY_APPOINTMENT_CANCEL, appointmentFinal);
                    emailService.handleEmail(MailTemplateEnum.CANCEL, appointmentFinal);
                } catch (MessagingException e) {
                    LOGGER.error("[cancelAppointment] Se ha producido un error al enviar el email:{}", e);
                }
            }).start();
        } else {
            LOGGER.error("[cancelAppointment] No existe ninguna cita con el id {}", idAppointment);
            throw new ErrorInfo(Constants.ERROR_TITLE_CANCEL_APPOINTMENT, Constants.ERROR_DESCRIPTION_CANCEL_APPOINTMENT, HttpStatus.NOT_FOUND.value());
        }
        return Mapper.convertAppointDBToAppointmentDTO(appointmentActive);
    }

    public List<AppointmentDTO> cancelMassiveAppointments(AppointmentCancelMassiveDTO appointmentCancelMassiveDTO) {
        List<AppointmentDTO> result = new ArrayList<>();

        // Se obtienen todas las citas de un día concreto
        List<Appointment> appointmentList = this.getAppointmentsByDateAndState(StateEnum.ACTIVA, appointmentCancelMassiveDTO.getDateCancelation());

        if (!appointmentList.isEmpty()) {
            // Se filtran las propias del peluquero y se cancela
            appointmentList = appointmentList.stream().filter(appointment -> appointment.getHairdresser().getId().equals(appointmentCancelMassiveDTO.getHairdresserId())).collect(Collectors.toList());
            for (Appointment appointment : appointmentList) {
                appointment.setState(State.builder().id(StateEnum.CANCELADA.getId()).build());
                appointmentDAO.save(appointment);

                appointment.setReason(appointmentCancelMassiveDTO.getReason());
                // Se envia la notificacion PUSH y el envío de correo
                final Appointment appointmentFinal = appointment;
                new Thread(() -> {
                    try {
                        notificationService.sendNotificationCancelMassiveAppointment(appointmentFinal, appointmentCancelMassiveDTO.getReason());
                        emailService.handleEmail(MailTemplateEnum.CANCEL_MASSIVE, appointmentFinal);
                    } catch (MessagingException e) {
                        LOGGER.error("[cancelAppointment] Se ha producido un error al enviar el email:{}", e);
                    }
                }).start();
            }
        }
        appointmentList.forEach(appointment -> result.add(Mapper.convertAppointDBToAppointmentDTO(appointment)));

        return result;
    }

    /**
     * Method that obtains appointments from a hairdresser
     *
     * @param filterDTO
     */
    public List<AppointmentDTO> getAppointmentsByHairdresserIdAndDate(AppointmentHairdresserFilterDTO filterDTO) {
        List<AppointmentDTO> appointmentDTOS = new ArrayList<>();
        List<Appointment> appointmentsDbList = this.appointmentDAO.getAppointmentsByHairdresserIdAndDate(filterDTO.getHairdresserId(),
                UtilDates.convertLocalDateToTimeStampSQL(filterDTO.getDateFrom()));
        appointmentsDbList.forEach(appointment ->
                appointmentDTOS.add(Mapper.convertAppointDBToAppointmentDTO(appointment)));
        LOGGER.info("Se han obtenido {} citas para el filtro {}", appointmentDTOS.size(), filterDTO);
        return appointmentDTOS;
    }

    /**
     * Method that obtains active appointments from a day by state.
     *
     * @param state
     * @param dateFrom
     * @return
     */
    public List<Appointment> getAppointmentsByDateAndState(StateEnum state, LocalDate dateFrom) {
        Timestamp dateSQL = UtilDates.convertLocalDateToTimeStampSQL(dateFrom);
        List<Appointment> appointmentList = this.appointmentDAO.getActiveAppointmentsByDateAndState(state.getId(), dateSQL);
        LOGGER.info("Se han obtenido {} citas {} para {}", appointmentList.size(), state.toString(), dateSQL);
        return appointmentList;
    }

    /**
     * Method that finalizes all appointments from today.
     */
    public void finishAllApointmentsToday() {
        // Se obtienen los ids de las citas activas
        List<Long> appointmentsIdsList = this.getAppointmentsByDateAndState(StateEnum.ACTIVA, LocalDate.now())
                .stream().map(Appointment::getId).collect(Collectors.toList());
        if (!appointmentsIdsList.isEmpty()) {
            int register = this.appointmentDAO.changeStateAppointments(StateEnum.FINALIZADA.getId(), appointmentsIdsList);
            LOGGER.info("Se han finalizado {} citas activas", register);
        }
    }

}
