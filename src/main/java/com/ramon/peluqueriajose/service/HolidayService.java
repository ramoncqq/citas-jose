package com.ramon.peluqueriajose.service;

import com.ramon.peluqueriajose.controller.HairdresserController;
import com.ramon.peluqueriajose.dto.HairdresserHolidayDTO;
import com.ramon.peluqueriajose.dto.constants.Constants;
import com.ramon.peluqueriajose.dto.error.ErrorInfo;
import com.ramon.peluqueriajose.model.Hairdresser;
import com.ramon.peluqueriajose.model.Holiday;
import com.ramon.peluqueriajose.repository.HolidayRepository;
import com.ramon.peluqueriajose.utils.Mapper;
import com.ramon.peluqueriajose.utils.UtilDates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class HolidayService {

    @Autowired
    private HolidayRepository holidayRepository;

    @Autowired
    private HairdresserService hairdresserService;

    private static final Logger LOGGER = LoggerFactory.getLogger(HolidayService.class);

    /**
     * Method that obtains all holidays from a hairdresser
     *
     * @param idHairdresser
     * @return
     * @throws ErrorInfo
     */
    public List<HairdresserHolidayDTO> getByIdHairdresser(Long idHairdresser) throws ErrorInfo {
        List<HairdresserHolidayDTO> holidayDTOList = new ArrayList<>();

        List<Holiday> holiday = holidayRepository.findByHairdresserId(idHairdresser);
        if (!holiday.isEmpty()) {
            for (Holiday ho : holiday) {
                holidayDTOList.add(Mapper.convertHolidayToHairdresserHolidayDTO(ho));
            }
        }
        return holidayDTOList;
    }

    /**
     * Method that save a list of holidays
     *
     * @param holidayList
     * @return
     * @throws ErrorInfo
     */
    public Set<Holiday> saveHolidays(List<HairdresserHolidayDTO> holidayList) throws ErrorInfo {
        Hairdresser hairdresser = hairdresserService.getHairdresserById(holidayList.get(0).getHairdresserId());
        Set<Holiday> holidays = new HashSet<>();
        if (hairdresser != null) {
            for (HairdresserHolidayDTO holiday : holidayList) {
                holidays.add(holidayRepository.save(Mapper.convertHairdresserHolidayToHoliday(holiday)));
            }
        } else {
            LOGGER.error("No se ha podido obtener al peluquero con id {}", holidayList.get(0).getHairdresserId());
            throw new ErrorInfo(Constants.ERROR_TITLE_HOLIDAY_CREATE, Constants.ERROR_DESCRIPTION_HOLIDAY_CREATE,
                    HttpStatus.NOT_FOUND.value());
        }
        return holidays;
    }


    /**
     * Nethod that cancel the holidays
     * @param holidayList
     */
    public void cancelHolidays(List<HairdresserHolidayDTO> holidayList) throws Exception {
        List<Holiday> holidayBack = holidayRepository.findByHairdresserId(holidayList.get(0).getHairdresserId());

        for (HairdresserHolidayDTO hairdresserHolidayDTO : holidayList) {
            String date = UtilDates.convertDateToString(hairdresserHolidayDTO.getStartDate(), Constants.FORMAT_DATE_APPOINTMENT);
            List<Holiday> holidayFilterList = holidayBack.stream().filter(ele -> ele.getStartDate().toString().equals(date)).collect(Collectors.toList());
            if (!holidayFilterList.isEmpty()) {
                LOGGER.info("Se cancela el día de vacaciones {} ", hairdresserHolidayDTO.getStartDate());
                holidayRepository.delete(holidayFilterList.get(0).getId());
            }
        }
    }

}
