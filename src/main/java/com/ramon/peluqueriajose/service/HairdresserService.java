package com.ramon.peluqueriajose.service;

import com.ramon.peluqueriajose.dto.error.ErrorInfo;
import com.ramon.peluqueriajose.model.Hairdresser;
import com.ramon.peluqueriajose.repository.HairdresserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class HairdresserService {

    @Autowired
    private HairdresserRepository hairdresserDAO;

    /**
     * Method that obtains all hairdressers
     *
     * @return
     */
    public List<Hairdresser> getHairdressers() {
        return hairdresserDAO.findAll();
    }

    /**
     * Method that obtains all hairdressers
     *
     * @param id
     * @return
     * @throws ErrorInfo
     */
    public Hairdresser getHairdresserById(Long id) throws ErrorInfo {
        return hairdresserDAO.findOne(id);
    }

}
