package com.ramon.peluqueriajose.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class JobService {

    private static Logger LOGGER = LoggerFactory.getLogger(JobService.class);

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private AppointmentService appointmentService;

    @Scheduled(cron = "${app.reminderAppointmentJob}")
    public void reminderAppointmentJob() {
        LOGGER.info("[sendNotificationAppointmentReminder] Se inicializa la tarea");
        this.notificationService.sendNotificationAppointmentReminder();
        LOGGER.info("[sendNotificationAppointmentReminder] Se finaliza la tarea");
    }

    @Scheduled(cron = "${app.finishAppointmentJob}")
    public void finishAllAppointmentsToday() {
        LOGGER.info("[finishAllAppointmentsToday] Se inicializa la tarea");
        this.appointmentService.finishAllApointmentsToday();
        LOGGER.info("[finishAllAppointmentsToday] Se finaliza la tarea");
    }


}
