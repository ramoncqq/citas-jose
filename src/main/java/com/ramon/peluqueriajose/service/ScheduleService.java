package com.ramon.peluqueriajose.service;

import com.ramon.peluqueriajose.dto.constants.Constants;
import com.ramon.peluqueriajose.model.Schedule;
import com.ramon.peluqueriajose.repository.ScheduleRepository;
import com.ramon.peluqueriajose.utils.UtilDates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ScheduleService {

    private Logger LOGGER = LoggerFactory.getLogger(ScheduleService.class);

    private static List<Schedule> scheduleList;

    @Autowired
    private ScheduleRepository scheduleDAO;

    @PostConstruct
    public void init() {
        LOGGER.info("Se obtienen los horarios");
        scheduleList = this.scheduleDAO.findAll();
    }

    /**
     * Method that returns the schedule of a day
     *
     * @param dayNumber the day number
     * @return schedule of a day
     */
    public Schedule getSchedule(Integer dayNumber) {
        Schedule scheduleDB = null;
        if (dayNumber < DayOfWeek.SUNDAY.getValue()) {
            scheduleDB = scheduleList.stream().filter(schedule -> schedule.getId().intValue() == dayNumber).collect(Collectors.toList()).get(0);
        }

        return scheduleDB;
    }

    /**
     * Method that calculate working hours from a day.
     *
     * @param dayNumber
     * @return
     */
    public Double calculateWorkingHours(Integer dayNumber) {
        Schedule scheduleDTO = this.getSchedule(dayNumber);
        double totalHours = 0;

        if (scheduleDTO != null) {
            if (!StringUtils.isEmpty(scheduleDTO.getMorning())) {
                LocalTime start = UtilDates.convertStringToLocalTime(scheduleDTO.getMorning().split(Constants.SEPARATOR_SCHEDULE)[0]);
                LocalTime end = UtilDates.convertStringToLocalTime(scheduleDTO.getMorning().split(Constants.SEPARATOR_SCHEDULE)[1]);
                totalHours += (float) Duration.between(start, end).toMinutes() / 60;
            }

            if (!StringUtils.isEmpty(scheduleDTO.getAfternoon())) {
                LocalTime start = UtilDates.convertStringToLocalTime(scheduleDTO.getAfternoon().split(Constants.SEPARATOR_SCHEDULE)[0]);
                LocalTime end = UtilDates.convertStringToLocalTime(scheduleDTO.getAfternoon().split(Constants.SEPARATOR_SCHEDULE)[1]);
                totalHours += (float) Duration.between(start, end).toMinutes() / 60;
            }
        }

        return totalHours;
    }
}
