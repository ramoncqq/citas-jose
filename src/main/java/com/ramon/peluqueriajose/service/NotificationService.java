package com.ramon.peluqueriajose.service;

import com.ramon.peluqueriajose.dto.AppointmentCancelMassiveDTO;
import com.ramon.peluqueriajose.dto.constants.MailTemplateEnum;
import com.ramon.peluqueriajose.dto.constants.StateEnum;
import com.ramon.peluqueriajose.dto.notification.NotificationDTO;
import com.ramon.peluqueriajose.dto.notification.NotificationPushDTO;
import com.ramon.peluqueriajose.dto.constants.Constants;
import com.ramon.peluqueriajose.dto.notification.NotificationResponseDTO;
import com.ramon.peluqueriajose.model.Appointment;
import com.ramon.peluqueriajose.utils.UtilDates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.mail.MessagingException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Component
public class NotificationService {

    private static Logger LOGGER = LoggerFactory.getLogger(NotificationService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private EmailService emailService;

    /**
     * Method that send notification  of create appointment
     *
     * @param title
     * @param body
     * @param appointment
     */
    public void sendNotificationToAdmin(String title, String body, Appointment appointment) {
        String fullName = appointment.getClient().getName() + " " + appointment.getClient().getSurname();
        String bodyNotification = String.format(body, fullName, UtilDates.convertDateToStringNotification(appointment.getDate()));
        // En caso de que tenga las noyificaciones activas
        if (appointment.getClient().getHairdresser().getNotification()) {
            NotificationPushDTO notification = NotificationPushDTO.builder()
                    .to(appointment.getClient().getHairdresser().getFcmToken())
                    .notification(NotificationDTO.builder().title(title)
                            .body(bodyNotification).build())
                    .build();

            LOGGER.info("[sendNotificationToAdmin] Se envia la notification con los siguientes datos {}", notification);
            ResponseEntity<NotificationResponseDTO> result = this.restTemplate.postForEntity(Constants.ENDPOINT_NOTIFICATION_PUSH, notification, NotificationResponseDTO.class);
            LOGGER.info("[sendNotificationToAdmin] Respuesta del servicio REST CODIGO :{}, BODY:{}", result.getStatusCode(), result.getBody());
        } else {
            LOGGER.error("[sendNotificationToAdmin] No se envia la notificación debido a que el peluquero {} tiene las notificaciones desactivadas",
                    appointment.getClient().getHairdresser().getName());
        }
    }

    /**
     * Method that send notification date appointment
     */
    public void sendNotificationAppointmentReminder() {
        List<Appointment> appointmentList =
                this.appointmentService.getAppointmentsByDateAndState(StateEnum.ACTIVA, LocalDate.now().plus(1, ChronoUnit.DAYS));

        // Una vez obtenidas las citas activas de mañana se envia la notificación
        appointmentList.forEach(appointment -> {
            if (appointment.getClient().getNotification()) {
                String fullName = appointment.getClient().getName() + " " + appointment.getClient().getSurname();
                String bodyNotification = String.format(Constants.NOTIFICATION_BODY_APPOINTMENT_DATE, fullName,
                        UtilDates.convertDateToStringNotification(appointment.getDate()));
                // Se crea el objeto para la notificación.
                NotificationPushDTO notification = NotificationPushDTO.builder()
                        .to(appointment.getClient().getFcmToken())
                        .notification(NotificationDTO.builder().title(Constants.NOTIFICATION_TITLE_APPOINTMENT_DATE)
                                .body(bodyNotification).build())
                        .build();
                LOGGER.info("[sendNotificationAppointmentReminder] Se envia la notification con los siguientes datos {}", notification);
                ResponseEntity<NotificationResponseDTO> result = this.restTemplate.postForEntity(Constants.ENDPOINT_NOTIFICATION_PUSH, notification, NotificationResponseDTO.class);
                LOGGER.info("Respuesta del servicio REST CODIGO :{}, BODY:{}", result.getStatusCode(), result.getBody());
            } else {
                LOGGER.error("[sendNotificationAppointmentReminder] No se envia la notificación debido a que el cliente {} no tiene las notificaciones activadas",
                        appointment.getClient().getEmail());
            }

            // Se envia el correo el electrónico
            try {
                this.emailService.handleEmail(MailTemplateEnum.REMINDER, appointment);
            } catch (MessagingException e) {
                LOGGER.error("Se ha producido un error al enviar el email: {}", e);
            }

        });
    }

    /**
     * Método que envía la notificación cuando el peluquero cancela la cita por una razón.
     *
     * @param appointment
     * @param reason
     */
    public void sendNotificationCancelMassiveAppointment(Appointment appointment, String reason) {
        String fullName = appointment.getHairdresser().getName();
        String bodyNotification = String.format("%s ha cancelado la cita del %s debido a %s", fullName, UtilDates.convertDateToStringNotification(appointment.getDate()), reason);

        // En caso de que tenga las notificaciones activas
        if (appointment.getClient().getNotification()) {
            NotificationPushDTO notification = NotificationPushDTO.builder()
                    .to(appointment.getClient().getFcmToken())
                    .notification(NotificationDTO.builder().title(Constants.NOTIFICATION_TITLE_APPOINTMENT_CANCEL)
                            .body(bodyNotification).build())
                    .build();

            LOGGER.info("[sendNotificationCancelMassiveAppointment] Se envia la notificacion con los siguientes datos {}", notification);
            ResponseEntity<NotificationResponseDTO> result = this.restTemplate.postForEntity(Constants.ENDPOINT_NOTIFICATION_PUSH, notification, NotificationResponseDTO.class);
            LOGGER.info("[sendNotificationCancelMassiveAppointment] Respuesta del servicio REST CODIGO :{}, BODY:{}", result.getStatusCode(), result.getBody());
        }
    }

}
