package com.ramon.peluqueriajose.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "app")
@Data
@Component
public class AppConfig {

    public int hourAppointment;

    public List<String> whiteList;

    private String jwtSecret;

    private int jwtExpirationInMs;

    private String host;
}
