package com.ramon.peluqueriajose.config;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

/** Filtro para enviar el key para el envio de notificaciones **/
public class HeaderRequestInterceptor implements ClientHttpRequestInterceptor {

    private final String HEADER_AUTHORIZATION_KEY = "Authorization";
    private final String HEADER_AUTHORIZATION_VALUE = "key=AIzaSyDLpqxjbhmRDrDi_kruAxCYSJXw_AjAuV0";

    @Override
    public ClientHttpResponse intercept(
            HttpRequest request,
            byte[] body,
            ClientHttpRequestExecution execution) throws IOException {

        request.getHeaders().add(HEADER_AUTHORIZATION_KEY, HEADER_AUTHORIZATION_VALUE);
        ClientHttpResponse response = execution.execute(request, body);
        return response;
    }
}