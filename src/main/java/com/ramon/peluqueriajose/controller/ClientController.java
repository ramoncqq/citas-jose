package com.ramon.peluqueriajose.controller;

import com.ramon.peluqueriajose.dto.ClientDetail;
import com.ramon.peluqueriajose.dto.constants.MailTemplateEnum;
import com.ramon.peluqueriajose.model.Appointment;
import com.ramon.peluqueriajose.model.Client;
import com.ramon.peluqueriajose.repository.AppointmentRepository;
import com.ramon.peluqueriajose.service.ClientService;
import com.ramon.peluqueriajose.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;

@RestController
@RequestMapping("/client")
public class ClientController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private AppointmentRepository AppointmentDAO;

    @Autowired
    private EmailService emailService;

    @GetMapping("/exists/{email:.+}")
    public ResponseEntity<?> checkClientExists(@PathVariable String email) {
        return new ResponseEntity<>(clientService.getClientDetailByEmail(email), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<?> createClient(@Valid @RequestBody ClientDetail client) {
        return new ResponseEntity<>(clientService.createClient(client), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> modifyClient(@Valid @RequestBody ClientDetail client) {
        return new ResponseEntity<>(clientService.modifyClient(client), HttpStatus.OK);
    }

    @GetMapping("/filter/{phone}")
    public ResponseEntity<?> findByPhone(@PathVariable String phone) {
        return new ResponseEntity<>(clientService.findByPhone(phone), HttpStatus.OK);
    }

    @PostMapping("/lock/{email:.+}")
    public ResponseEntity<?> lockUser(@PathVariable String email) {
        return new ResponseEntity<>(clientService.lockUser(email), HttpStatus.OK);
    }

    @PostMapping("/unlock/{email:.+}")
    public ResponseEntity<?> unlockUser(@PathVariable String email) {
        return new ResponseEntity<>(clientService.unlockUser(email), HttpStatus.OK);
    }

    @GetMapping("/appointment/email/{email:.+}")
    public ResponseEntity<?> getAppointmentsByEmailUser(@PathVariable("email") String email) {
        return new ResponseEntity<>(clientService.getAppointmentsByEmail(email), HttpStatus.OK);
    }

    @GetMapping("/appointment/phone/{phone}")
    public ResponseEntity<?> getAppointmentsBPyPhone(@PathVariable("phone") String phone) {
        return new ResponseEntity<>(clientService.getAppointmentsByPhone(phone), HttpStatus.OK);
    }

    @GetMapping("/test")
    public ResponseEntity<?> xxx(@RequestParam("template") MailTemplateEnum mailTemplateEnum) throws MessagingException {
        Appointment appointment = this.AppointmentDAO.findOne(13L);
        emailService.handleEmail(mailTemplateEnum, appointment);
        return new ResponseEntity<>("aa", HttpStatus.OK);
    }


}
