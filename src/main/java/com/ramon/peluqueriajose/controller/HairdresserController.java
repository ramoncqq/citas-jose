package com.ramon.peluqueriajose.controller;

import com.ramon.peluqueriajose.dto.AppointmentHairdresserFilterDTO;
import com.ramon.peluqueriajose.service.AppointmentService;
import com.ramon.peluqueriajose.service.HairdresserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/hairdresser")
public class HairdresserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HairdresserController.class);

    @Autowired
    private HairdresserService hairdresserService;

    @Autowired
    private AppointmentService appointmentService;

    @GetMapping("/")
    public ResponseEntity<?> getHairdressers() {
        return new ResponseEntity<>(hairdresserService.getHairdressers(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getHairdressers(@PathVariable("id") Long id) {
        return new ResponseEntity<>(hairdresserService.getHairdresserById(id), HttpStatus.OK);
    }

    @PostMapping("/appointment")
    public ResponseEntity<?> getAppointmentsByHairdresserIdAndDate(@Valid @RequestBody AppointmentHairdresserFilterDTO filterDTO) {
        return new ResponseEntity<>(appointmentService.getAppointmentsByHairdresserIdAndDate(filterDTO), HttpStatus.OK);
    }

}
