package com.ramon.peluqueriajose.controller;

import com.ramon.peluqueriajose.dto.AvailabilityAppointmentFilterDTO;
import com.ramon.peluqueriajose.dto.HoursAvailabilityFilterDTO;
import com.ramon.peluqueriajose.service.AvailabilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.text.ParseException;

@RestController
@RequestMapping("/availability")
public class AvailabilityController {

    @Autowired
    private AvailabilityService availabilityService;

    @PostMapping("/hours")
    public ResponseEntity<?> getHoursAvalability(@Valid @RequestBody HoursAvailabilityFilterDTO filter) {
        return new ResponseEntity<>(availabilityService.getAvailableHoursFromDate(filter), HttpStatus.OK);
    }

    @PostMapping("/dates")
    public ResponseEntity<?> getAvailabilityAppointmentsBetweenTwoDates(@Valid @RequestBody AvailabilityAppointmentFilterDTO filterDTO) throws ParseException {
        return new ResponseEntity<>(availabilityService.getAvailabilityAppointmentsBetweenTwoDates(filterDTO), HttpStatus.OK);
    }


}
