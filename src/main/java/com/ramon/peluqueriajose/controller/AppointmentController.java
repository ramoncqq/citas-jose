package com.ramon.peluqueriajose.controller;

import com.ramon.peluqueriajose.dto.AppointmentCancelMassiveDTO;
import com.ramon.peluqueriajose.dto.AppointmentCreate;
import com.ramon.peluqueriajose.dto.AvailabilityAppointmentFilterDTO;
import com.ramon.peluqueriajose.dto.HoursAvailabilityFilterDTO;
import com.ramon.peluqueriajose.dto.constants.Constants;
import com.ramon.peluqueriajose.service.AppointmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.time.LocalDate;

@RestController
@RequestMapping("/appointment")
public class AppointmentController {

    @Autowired
    private AppointmentService appointmentService;

    @PostMapping
    public ResponseEntity<?> createAppointment(@Valid @RequestBody AppointmentCreate appointmentCreate,
                                               @RequestParam(defaultValue = "false", value = "isAdmin") boolean isAdmin) {
        return new ResponseEntity<>(appointmentService.createAppointment(appointmentCreate, isAdmin), HttpStatus.CREATED);
    }

    @PostMapping("/finish/{idAppointment}")
    public ResponseEntity<?> finishAppointment(@PathVariable Long idAppointment) {
        return new ResponseEntity<>(appointmentService.finishAppointment(idAppointment), HttpStatus.OK);
    }

    @PostMapping("/cancel/{idAppointment}")
    public ResponseEntity<?> cancelAppointment(@PathVariable Long idAppointment) {
        return new ResponseEntity<>(appointmentService.cancelAppointment(idAppointment), HttpStatus.OK);
    }

    @PostMapping("/cancelMassive")
    public ResponseEntity<?> cancelMassiveAppointments(@RequestBody AppointmentCancelMassiveDTO appointmentCancelMassiveDTO) {
        return new ResponseEntity<>(appointmentService.cancelMassiveAppointments(appointmentCancelMassiveDTO), HttpStatus.OK);
    }

}
