package com.ramon.peluqueriajose.controller;

import com.ramon.peluqueriajose.dto.HairdresserHolidayDTO;
import com.ramon.peluqueriajose.service.HolidayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/holiday")
public class HolidayController {

    @Autowired
    private HolidayService holidayService;

    @PostMapping
    public ResponseEntity<?> createHolidays(@Valid @RequestBody List<HairdresserHolidayDTO> holidayList) {
        return new ResponseEntity<>(holidayService.saveHolidays(holidayList), HttpStatus.CREATED);
    }

    @GetMapping("/{hairdresserId}")
    public ResponseEntity<?> getHolidays(@Valid @PathVariable("hairdresserId") Long hairdresserId) {
        return new ResponseEntity<>(holidayService.getByIdHairdresser(hairdresserId), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<?> cancelHolidays(@Valid @RequestBody List<HairdresserHolidayDTO> holidayList) throws Exception {
        try {
            holidayService.cancelHolidays(holidayList);
        } catch (Exception e) {
            System.out.println(e);
        }
        return new ResponseEntity<>(holidayList, HttpStatus.OK);
    }


}
