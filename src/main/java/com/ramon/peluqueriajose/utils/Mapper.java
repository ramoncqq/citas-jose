package com.ramon.peluqueriajose.utils;

import com.ramon.peluqueriajose.dto.AppointmentDTO;
import com.ramon.peluqueriajose.dto.ClientDetail;
import com.ramon.peluqueriajose.dto.HairdresserHolidayDTO;
import com.ramon.peluqueriajose.dto.constants.StateEnum;
import com.ramon.peluqueriajose.model.Appointment;
import com.ramon.peluqueriajose.model.Client;
import com.ramon.peluqueriajose.model.Hairdresser;
import com.ramon.peluqueriajose.model.Holiday;
import com.ramon.peluqueriajose.security.dto.UserDTO;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.stream.Collectors;

public class Mapper {

    public static AppointmentDTO convertAppointDBToAppointmentDTO(Appointment appointmentDB) {
        return AppointmentDTO.builder()
                .id(appointmentDB.getId())
                .hairdresserName(appointmentDB.getHairdresser().getName())
                .serviceName(appointmentDB.getService().getName()).date(appointmentDB.getDate())
                .email(appointmentDB.getClient().getEmail())
                .name(appointmentDB.getClient().getName())
                .surname(appointmentDB.getClient().getSurname())
                .state(StateEnum.getById(appointmentDB.getState().getId())).build();
    }

    public static ClientDetail convertClientDBToClientDetail(Client client) {
        return ClientDetail.builder().email(client.getEmail()).surname(client.getSurname()).name(client.getName())
                .phone(client.getPhone()).active(client.getActive())
                .hairdresserId(client.getHairdresser().getId())
                .notification(client.getNotification()).build();
    }

    public static Client convertClientDetailToClient(ClientDetail clientDetail) {
        return Client.builder().email(clientDetail.getEmail()).surname(clientDetail.getSurname()).name(clientDetail.getName())
                .phone(clientDetail.getPhone()).fcmToken(clientDetail.getFcmToken())
                .notification(clientDetail.getNotification()).build();
    }

    public static void modifyClientDB(ClientDetail clientDetail, Client client) {
        client.setEmail(clientDetail.getEmail());
        client.setName(clientDetail.getName());
        client.setSurname(clientDetail.getSurname());
        client.setPhone(clientDetail.getPhone());
        client.setNotification(clientDetail.getNotification());
        client.setFcmToken(clientDetail.getFcmToken());
    }

    public static UserDTO convertClientToUserSecurity(Client client) {
        return UserDTO.builder()
                .username(client.getEmail())
                .password(client.getPhone())
                .authorities(client.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList()))
                .build();
    }

    public static Holiday convertHairdresserHolidayToHoliday(HairdresserHolidayDTO hairdresserHolidayDTO) {
        return Holiday.builder().startDate(hairdresserHolidayDTO.getStartDate())
                                .hairdresser(Hairdresser.builder().id(hairdresserHolidayDTO.getHairdresserId()).build()).build();
    }

    public static HairdresserHolidayDTO convertHolidayToHairdresserHolidayDTO(Holiday holiday) {
        return HairdresserHolidayDTO.builder().hairdresserId(holiday.getId()).startDate(holiday.getStartDate()).build();
    }

}
