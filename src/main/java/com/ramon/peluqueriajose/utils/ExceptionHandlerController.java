package com.ramon.peluqueriajose.utils;

import com.ramon.peluqueriajose.dto.error.ErrorInfo;
import com.ramon.peluqueriajose.dto.error.GlobalError;
import io.jsonwebtoken.ExpiredJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ControllerAdvice
public class ExceptionHandlerController {

    static Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerController.class);

    @ExceptionHandler(value = {ErrorInfo.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public GlobalError unauthorized(ErrorInfo e) {
        return new GlobalError(e.getMessage(), e.getDescription(), e.getStatusCode());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = RuntimeException.class)
    public GlobalError handleBaseException(RuntimeException e) {
        LOGGER.error("{}", e);
        return new GlobalError("RuntimeException", "RunTimeException", HttpStatus.BAD_REQUEST.value());
    }


    @ExceptionHandler(value = {AuthenticationException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public GlobalError unauthorized(AuthenticationException e) {
        LOGGER.error("{}", e);
        return new GlobalError("Authentication Error", "AuthenticationError", HttpStatus.UNAUTHORIZED.value());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public GlobalError genericHandlerError(Exception e) {
        LOGGER.error("Error no controlado", e);
        return new GlobalError("Generic Error", e.getMessage(), 500);
    }

    @ExceptionHandler(value = {HttpRequestMethodNotSupportedException.class})
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public GlobalError handleMethodNotAllowed(HttpRequestMethodNotSupportedException e) {
        LOGGER.error("{}", e);
        return new GlobalError("Method Not Allowed", "Method Not Allowed", HttpStatus.METHOD_NOT_ALLOWED.value());
    }

    @ExceptionHandler(value = {HttpMediaTypeNotSupportedException.class})
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    public GlobalError handlemediaTypeNotSupported(HttpMediaTypeNotSupportedException e) {
        return new GlobalError("Media Type Not Supported", "Media Type Not Supported", HttpStatus.UNSUPPORTED_MEDIA_TYPE.value());
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public GlobalError notReadableException(MethodArgumentNotValidException e) {
        LOGGER.error("{}", e);
        return new GlobalError("Http Message Not Readable Exception", e.getMessage(), HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public GlobalError notReadableException(AccessDeniedException e) {
        LOGGER.error("{}", e);
        return new GlobalError("Acceso denegado al recurso", e.getMessage(), HttpStatus.FORBIDDEN.value());
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public GlobalError notReadableException(BadCredentialsException e) {
        LOGGER.error("{}", e);
        return new GlobalError("Bad credentials", e.getMessage(), HttpStatus.UNAUTHORIZED.value());
    }

    @ExceptionHandler(ExpiredJwtException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public GlobalError tokenExpirated(ExpiredJwtException e) {
        LOGGER.error("Token expirado {}", e.getMessage());
        return new GlobalError("Token expirado", e.getMessage(), HttpStatus.UNAUTHORIZED.value());
    }

}
