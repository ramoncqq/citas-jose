package com.ramon.peluqueriajose.utils;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.ramon.peluqueriajose.dto.constants.Constants;

public class DateDeserializer extends StdDeserializer<Date> {

    private static final long serialVersionUID = 4646104434402077418L;

    protected DateDeserializer() {
        super(Date.class);
    }

    @Override
    public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        Date result = null;
        String value = jp.readValueAs(String.class);
        try {
            result = UtilDates.convertStringToDate(value, Constants.FORMAT_DATE_APPOINTMENT_WITH_HOUR_MINUTE);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }
}
