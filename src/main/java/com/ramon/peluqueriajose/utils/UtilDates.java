package com.ramon.peluqueriajose.utils;

import com.ramon.peluqueriajose.dto.constants.Constants;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;

public class UtilDates {

    public static Date convertStringToDate(String date, String format) throws ParseException {
        SimpleDateFormat sd = new SimpleDateFormat(format);
        return sd.parse(date);
    }

    public static String convertDatetoString(Date date) throws ParseException {
        SimpleDateFormat sd = new SimpleDateFormat(Constants.FORMAT_DATE_APPOINTMENT_WITH_HOUR_MINUTE);
        return sd.format(date);
    }

    public static String convertDateToString(Date date, String format) throws ParseException {
        SimpleDateFormat sd = new SimpleDateFormat(format);
        return sd.format(date);
    }

    public static Integer getDayWeekFromDateString(String dateString) {
        DateTimeFormatter dTF = DateTimeFormatter.ofPattern(Constants.FORMAT_DATE_APPOINTMENT);
        return LocalDate.parse(dateString, dTF).getDayOfWeek().getValue();
    }

    public static Timestamp convertLocalDateToTimeStampSQL(LocalDate dateIn) {
        return Timestamp.valueOf(dateIn.atStartOfDay());
    }

    public static Timestamp convertDateToTimeStampSQL(Date dateIn) {
        return new Timestamp(dateIn.getTime());
    }

    public static String convertDateToStringNotification(Date dateIn) {
        SimpleDateFormat sd = new SimpleDateFormat(Constants.FORMAT_DATE_NOTIFICATION);
        return sd.format(dateIn);
    }

    public static String getHourAndMinuteFromDate(Date dateIn) {
        SimpleDateFormat sd = new SimpleDateFormat(Constants.FORMAT_HOUR_MINUTE);
        return sd.format(dateIn);
    }

    public static boolean checkBetween(Date dateToCheck, Date startDate, Date endDate) {
        return dateToCheck.compareTo(startDate) >= 0 && dateToCheck.compareTo(endDate) <= 0;
    }

    public static boolean checkLocalTime(Date dateToCheck, LocalTime currentHour) {
        LocalTime ldt = LocalDateTime.ofInstant(dateToCheck.toInstant(), ZoneId.systemDefault()).toLocalTime();
        return currentHour.equals(ldt);
    }

    public static LocalTime convertStringToLocalTime(String hour) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.FORMAT_HOUR_MINUTE);
        return LocalTime.parse(hour, formatter);
    }

    public static LocalDate convertDateToLocalDate(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static Date convertLocalDateToDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

}
