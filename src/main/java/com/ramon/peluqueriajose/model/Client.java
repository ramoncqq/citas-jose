package com.ramon.peluqueriajose.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "client")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = "appointments")
public class Client {

    private static final long serialVersionUID = 1L;

    @GenericGenerator(
            name = "clientSequenceGenerator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "CLIENT_SEQUENCE"),
                    @Parameter(name = "initial_value", value = "0"),
                    @Parameter(name = "increment_size", value = "1")
            }
    )
    @Id
    @GeneratedValue(generator = "clientSequenceGenerator")
    @Column(name = "client_id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    @NotNull
    @NotEmpty
    private String name;

    @Column(name = "email", nullable = false)
    @NotNull
    @NotEmpty
    private String email;

    @Column(name = "surname", nullable = false)
    @NotNull
    @NotEmpty
    private String surname;

    @Column(name = "active", nullable = false)
    private Boolean active;

    @Column(name = "phone", nullable = false, length = 9)
    @Pattern(regexp = "\\d{9}", message = "El teléfono únicamente debe tener dígitos")
    @NotNull
    @NotEmpty
    private String phone;

    @Column(name = "fcm_token")
    private String fcmToken;

    @Column(name = "notification")
    private Boolean notification;

    @Column(name = "is_hairdresser", nullable = false)
    private Boolean isHairdresser = Boolean.FALSE;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(name = "client_role",
            joinColumns = @JoinColumn(name = "client_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, mappedBy = "client")
    private List<Appointment> appointments;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "hairdresser_id")
    private Hairdresser hairdresser;

    public String getName() {
        return this.name.toUpperCase();
    }

    public void setName(String name) {
        this.name = name.trim().toUpperCase();
    }

    public String getSurname() {
        return this.surname.toUpperCase();
    }

    public void setSurname(String surname) {
        this.surname = surname.trim().toUpperCase();
    }

}
