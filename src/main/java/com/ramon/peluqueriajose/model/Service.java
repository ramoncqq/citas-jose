package com.ramon.peluqueriajose.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "service")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Service {

    @GenericGenerator(
            name = "serviceSequenceGenerator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "SERVICE_SEQUENCE"),
                    @Parameter(name = "initial_value", value = "0"),
                    @Parameter(name = "increment_size", value = "1")
            }
    )
    @Id
    @GeneratedValue(generator = "serviceSequenceGenerator")
    @Column(name = "service_id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

}
