package com.ramon.peluqueriajose.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "schedule")
@Data
public class Schedule {

    @GenericGenerator(
            name = "scheduleSequenceGenerator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "SCHEDULE_SEQUENCE"),
                    @Parameter(name = "initial_value", value = "0"),
                    @Parameter(name = "increment_size", value = "1")
            }
    )
    @Id
    @GeneratedValue(generator = "scheduleSequenceGenerator")
    @Column(name = "schedule_id", nullable = false)
    private Long id;

    @Column(name = "morning", length = 11)
    private String morning;

    @Column(name = "afternoon", length = 11)
    private String afternoon;

}
