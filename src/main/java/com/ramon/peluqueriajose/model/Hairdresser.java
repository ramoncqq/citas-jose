package com.ramon.peluqueriajose.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "hairdresser")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Hairdresser {

    @GenericGenerator(
            name = "hairdresserSequenceGenerator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "HAIRDRESSER_SEQUENCE"),
                    @Parameter(name = "initial_value", value = "0"),
                    @Parameter(name = "increment_size", value = "1")
            }
    )
    @Id
    @GeneratedValue(generator = "hairdresserSequenceGenerator")
    @Column(name = "hairdresser_id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "fcm_token", nullable = false)
    private String fcmToken;

    @Column(name = "notification", nullable = false)
    private Boolean notification;

    @Column(name = "email", nullable = false)
    private String email;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "hairdresser_service",
            joinColumns = @JoinColumn(name = "hairdresser_id"),
            inverseJoinColumns = @JoinColumn(name = "service_id"))
    private Set<Service> services;

}
