package com.ramon.peluqueriajose.repository;

import com.ramon.peluqueriajose.model.Holiday;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HolidayRepository extends CrudRepository<Holiday, Long> {

    List<Holiday> findByHairdresserId(Long hairdresserId);

}
