package com.ramon.peluqueriajose.repository;

import com.ramon.peluqueriajose.model.Schedule;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScheduleRepository extends CrudRepository<Schedule, Long> {

    @Override
    List<Schedule> findAll();
}
