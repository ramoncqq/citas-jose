package com.ramon.peluqueriajose.repository;

import com.ramon.peluqueriajose.model.Hairdresser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HairdresserRepository extends CrudRepository<Hairdresser, Long> {

    @Override
    List<Hairdresser> findAll();

}