package com.ramon.peluqueriajose.repository;

import com.ramon.peluqueriajose.dto.constants.Querys;
import com.ramon.peluqueriajose.model.Client;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {

    @Query(Querys.CLIENT_EXITS)
    Integer findByEmailOrPhone(@Param("email") String email, @Param("phone") String phone);

    @Query(Querys.PHONE_NUMBER_EXIST)
    Integer existsPhoneInOtherClient(@Param("email") String email,  @Param("phone") String phone);

    Client findByEmail(String email);

    Client findByPhone(String phone);

    Client findByEmailAndPhone(String email, String phone);

}
