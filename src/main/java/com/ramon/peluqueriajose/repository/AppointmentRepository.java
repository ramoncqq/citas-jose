package com.ramon.peluqueriajose.repository;

import com.ramon.peluqueriajose.dto.constants.Querys;
import com.ramon.peluqueriajose.model.Appointment;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Repository
public interface AppointmentRepository extends CrudRepository<Appointment, Long> {

    @Query(Querys.ACTIVE_APPOINTMENT)
    Appointment getActiveAppointmentUser(@Param("stateID") Integer stateID, @Param("email") String email);

    @Query(Querys.HAIRDRESSER_APPOINTMENTS_BETWEEN_TWO_DATES)
    List<Appointment> getAppointmentsBetweenTwoDates(@Param("hairdresserId") Long hairdresserId,
                                                     @Param("dateFrom") Timestamp dateFrom,
                                                     @Param("dateTo") Timestamp dateTo);

    @Query(Querys.HAIRDRESSER_APPOINTMENTS)
    List<Appointment> getAppointmentsByHairdresserIdAndDate(@Param("hairdresserId") Long hairdresserId,
                                                            @Param("dateFrom") Timestamp dateFrom);

    @Query(Querys.APPOINTMENTS_BY_DAY_AND_STATE)
    List<Appointment> getActiveAppointmentsByDateAndState(@Param("stateID") Integer stateId,
                                                          @Param("dateFrom") Timestamp dateFrom);

    @Query(Querys.APPOINTMENTS_EXISTS)
    Long checkExistAppointment(@Param("stateList") List<Integer> stateList,
                               @Param("date") Timestamp date,
                               @Param("hairdresserId") Long hairdresserId);

    @Query(Querys.CHANGE_STATE_APPOINTMENTS)
    @Modifying
    @Transactional
    int changeStateAppointments(@Param("stateID") Integer stateId,
                                 @Param("appointmentsIdsList") List<Long> appointmentsIdsList);

}
