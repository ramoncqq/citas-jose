
-- SECUENCIAS

create sequence holiday_sequence
start with 0
increment by 1
maxvalue 9999999
minvalue 0
cycle;

create sequence hairdresser_sequence
start with 0
increment by 1
maxvalue 9999999
minvalue 0
cycle;

create sequence state_sequence
start with 0
increment by 1
maxvalue 9999999
minvalue 0
cycle;

create sequence service_sequence
start with 0
increment by 1
maxvalue 9999999
minvalue 0
cycle;

create sequence schedule_sequence
start with 1
increment by 1
maxvalue 9999999
minvalue 0
cycle;

create sequence role_sequence
start with 0
increment by 1
maxvalue 9999999
minvalue 0
cycle;

create sequence client_sequence
start with 0
increment by 1
maxvalue 9999999
minvalue 0
cycle;

create sequence appointment_sequence
start with 0
increment by 1
maxvalue 9999999
minvalue 0
cycle;

CREATE TABLE IF NOT EXISTS schedule(
 schedule_id integer PRIMARY KEY,
 morning VARCHAR(11),
 afternoon VARCHAR(11)
);

CREATE TABLE IF NOT EXISTS holiday(
    holiday_id INTEGER PRIMARY KEY,
    start_date date NOT NULL,
    hairdresser_id INTEGER,
    CONSTRAINT FK_holiday_hairdresser_id FOREIGN KEY(hairdresser_id) REFERENCES hairdresser (hairdresser_id)
);

CREATE TABLE IF NOT EXISTS service(
 service_id INTEGER PRIMARY KEY,
 name VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS hairdresser (
  hairdresser_id INTEGER PRIMARY KEY,
  name VARCHAR NOT NULL,
  fcm_token VARCHAR NOT NULL ,
  notification boolean NOT NULL,
  email VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS hairdresser_service(
 hairdresser_id INTEGER,
 service_id INTEGER,
 CONSTRAINT FK_hairdresser_hairdresser_id FOREIGN KEY(hairdresser_id) REFERENCES hairdresser (hairdresser_id),
 CONSTRAINT FK_hairdresser_service_id FOREIGN KEY(service_id) REFERENCES service (service_id)
);

CREATE TABLE IF NOT EXISTS state (
  state_id integer PRIMARY KEY,
  name varchar NOT NULL
);

CREATE TABLE IF NOT EXISTS client (
  client_id integer PRIMARY KEY,
  email VARCHAR NOT NULL,
  phone VARCHAR(9) NOT NULL,
  name VARCHAR NOT NULL,
  surname VARCHAR NOT NULL,
  active boolean NOT NULL,
  fcm_token VARCHAR NOT NULL,
  notification boolean NOT NULL,
  is_hairdresser boolean DEFAULT FALSE NOT NULL,
  hairdresser_id INTEGER REFERENCES hairdresser(hairdresser_id)
);

CREATE TABLE IF NOT EXISTS appointment (
  appointment_id integer PRIMARY KEY,
  client_id  integer REFERENCES client(client_id) ON DELETE CASCADE,
  date TIMESTAMP NOT NULL,
  state_id integer REFERENCES state(state_id),
  hairdresser_id INTEGER REFERENCES hairdresser(hairdresser_id),
  service_id INTEGER REFERENCES service(service_id)
);

CREATE TABLE IF NOT EXISTS role (
    role_id integer PRIMARY KEY,
    name VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS client_role(
 client_id INTEGER,
 role_id INTEGER,
 CONSTRAINT FK_client_client_id FOREIGN KEY(client_id) REFERENCES client (client_id) ON DELETE CASCADE,
 CONSTRAINT FK_client_role_id FOREIGN KEY(role_id) REFERENCES role (role_id) ON DELETE CASCADE
);

INSERT INTO state(state_id, name) VALUES (nextval('state_sequence'), 'ACTIVA');
INSERT INTO state(state_id, name) VALUES (nextval('state_sequence'), 'CANCELADA');
INSERT INTO state(state_id, name) VALUES (nextval('state_sequence'), 'TERMINADA');
INSERT INTO schedule(schedule_id, afternoon) VALUES (nextval('schedule_sequence'), '16:00-19:45');
INSERT INTO schedule(schedule_id, morning, afternoon) VALUES (nextval('schedule_sequence'), '10:00-13:00', '16:30-20:00');
INSERT INTO schedule(schedule_id, morning, afternoon) VALUES (nextval('schedule_sequence'), '10:00-13:00', '16:30-20:00');
INSERT INTO schedule(schedule_id, morning, afternoon) VALUES (nextval('schedule_sequence'), '10:00-13:00', '16:30-20:00');
INSERT INTO schedule(schedule_id, morning, afternoon) VALUES (nextval('schedule_sequence'), '10:00-13:00', '16:30-20:00');
INSERT INTO schedule(schedule_id, morning) VALUES (nextval('schedule_sequence'), '10:00-13:45');
INSERT INTO hairdresser(hairdresser_id, name, fcm_token, notification) VALUES (nextval('hairdresser_sequence'), 'Jose', 'x', true);
INSERT INTO hairdresser(hairdresser_id, name, fcm_token, notification) VALUES (nextval('hairdresser_sequence'), 'Fany', 'x', true);
INSERT INTO service(service_id, name) VALUES (nextval('service_sequence'), 'Corte de pelo');
INSERT INTO service(service_id, name) VALUES (nextval('service_sequence'), 'Cejas');
INSERT INTO hairdresser_service (hairdresser_id, service_id) VALUES (0, 0);
INSERT INTO hairdresser_service (hairdresser_id, service_id) VALUES (1, 0);
INSERT INTO hairdresser_service (hairdresser_id, service_id) VALUES (1, 1);
INSERT INTO role VALUES(nextval('role_sequence'), 'ADMIN');
INSERT INTO role VALUES(nextval('role_sequence'), 'USER');
INSERT INTO client(client_id, name, surname ,email, phone, active, fcm_token, notification, hairdresser_id) VALUES(nextval('client_sequence'), 'Ramon', 'Carmona Quintero', 'ramoncqq@gmail.com', '699025403', true, 'x', true, 0);
INSERT INTO client_role VALUES(0, 0);

-- DROP
DROP SEQUENCE APPOINTMENT_SEQUENCE;
DROP SEQUENCE CLIENT_SEQUENCE;
DROP SEQUENCE HAIRDRESSER_SEQUENCE;
DROP SEQUENCE HOLIDAY_SEQUENCE;
DROP SEQUENCE ROLE_SEQUENCE;
DROP SEQUENCE SCHEDULE_SEQUENCE;
DROP SEQUENCE SERVICE_SEQUENCE;
DROP SEQUENCE STATE_SEQUENCE;

DROP TABLE APPOINTMENT CASCADE;
DROP TABLE CLIENT CASCADE;
DROP TABLE CLIENT_ROLE CASCADE;
DROP TABLE HAIRDRESSER CASCADE;
DROP TABLE HAIRDRESSER_SERVICE CASCADE;
DROP TABLE HOLIDAY CASCADE;
DROP TABLE ROLE CASCADE;
DROP TABLE SCHEDULE CASCADE;
DROP TABLE SERVICE CASCADE;
DROP TABLE STATE CASCADE;